#!env python3
from collections import Counter
from pprint import pformat
import datetime
from builtins import print
import bitstring

dev = True g
log_file = 'shkib_1000.csv'
log_file = 'shkib_50.csv'
log_file = 'shkib.csv'

result_file = 'result.txt'

_time = 0
src_user = 1
src_ip = 2
src_port = 3
dest_user = 4
dest_ip = 5
dest_port = 6
input_byte = 7
output_byte = 8

from utils import *


def read_log():
    with open(log_file, 'r', encoding='utf-8') as file:
        for line in file:
            # sort data
            if line[:3] == '"_t':
                continue
            token = line.split(',')
            token[0] = token[0].strip('"')
            token[8] = token[8].strip('\n')
            yield token


def solve_task_1_2():
    users_counter = Counter()
    output_byte_counter = Counter()
    # task1
    for token in read_log():
        users_counter[token[1]] += 1
        # task2
        output_byte_counter[token[1]] += int(token[8])
    return users_counter.most_common(5), output_byte_counter.most_common(5)


def solve_task3_4(taskNumber):
    requsts_by_user = {}
    if taskNumber == 3:
        search_filter_token = src_user
    else:
        search_filter_token = src_ip
    if dev:
        import pickle
        with open('task3.pickle', 'rb') as f:
            requsts_by_user = pickle.load(f)
    else:
        for token in read_log():
            log_time = from_iso8601(token[_time], remove_zone=True)
            user = token[search_filter_token]

            if user not in requsts_by_user:
                requsts_by_user[user] = set()
            requsts_by_user[user].add(log_time)

        for user in requsts_by_user:
            requsts_by_user[user] = list(requsts_by_user[user])
            requsts_by_user[user].sort()

    min_delay = 3  # 0 for all result
    max_delay = None  # None for all result
    max_result = 0  # 0 - all result
    min_pereodic = 20
    pereodic_requests_by_user = dict.fromkeys(requsts_by_user.keys(), [])

    zero = datetime.timedelta(0)
    epoch = datetime.datetime.utcfromtimestamp(0)
    start_time = 1516296410
    end_time = 1516390370

    start_time_object = datetime.datetime.fromtimestamp(start_time)
    end_time_object = datetime.datetime.fromtimestamp(end_time)

    def to_relative(time):
        if isinstance(time, datetime.timedelta):
            return time.seconds
        return (time - start_time_object).seconds

    time_range = end_time - start_time

    user_count = 0
    for user, logs_timeline in requsts_by_user.items():
        user_count += 1
        if user_count % 10 == 0:
            print('corrent user: {}'.format(user_count))
        time_surface = [False for x in range(0, time_range)]

        # key feature: move absoltute time to relative time in array of all seconds between first and last log events
        for time in logs_timeline:
            time_surface[to_relative(time)] = True

        # key feature #2: 2-D bits matrix for save handled points and do not process their again
        handled_points = [bitstring.BitArray(time_range) for x in range(time_range)]

        for a in range(len(logs_timeline) - 1):
            A = logs_timeline[a]
            for b in range(a + 1, len(logs_timeline) - 1):
                B = logs_timeline[b]
                init_step = to_relative(B - A)
                if handled_points[a][b]:
                    continue
                handled_points[a][b] = True
                if init_step < min_delay or (max_delay is not None and init_step > max_delay):
                    continue
                count = 0
                for next_point in range(to_relative(B) + init_step, len(time_surface), init_step):
                    handled_points[next_point - init_step][next_point] = True
                    if time_surface[next_point]:
                        last_step = next_point
                        count += 1
                    else:
                        break
                if count >= min_pereodic:
                    # print('count={}, step={}, time={}'.format(count, first_step_rel, iso_8601(A)))
                    pereodic_requests_by_user[user].append((count, init_step, iso_8601(A)))

    for user, pereodic_requests in pereodic_requests_by_user.items():
        pereodic_requests.sort()
        pereodic_requests.reverse()
        pereodic_requests_by_user[user] = pereodic_requests[:max_result]
    return pformat(pereodic_requests_by_user)


def solve_task5():
    temp_ngrams = []
    ngrams = Counter()
    log_count = 0
    for token in read_log():
        log_count += 1
        temp_ngrams.append(','.join([token[src_user], token[src_port], token[dest_ip], token[dest_port]]))
        if len(temp_ngrams) < 5:
            if len(temp_ngrams) < 3:
                continue
            if len(temp_ngrams) == 3:
                ngrams[';'.join(temp_ngrams)] += 1
            if len(temp_ngrams) == 4:
                ngrams[';'.join(temp_ngrams[:4])] += 1
                ngrams[';'.join(temp_ngrams[1:4])] += 1
        else:
            ngrams[';'.join(temp_ngrams)] += 1
            ngrams[';'.join(temp_ngrams[1:])] += 1
            ngrams[';'.join(temp_ngrams[2:])] += 1
            temp_ngrams.pop(0)

    # pprint(ngrams.most_common(5))
    return ngrams.most_common(5), log_count


if __name__ == '__main__':
    with open(result_file, 'w', encoding='utf-8') as file:
        file.writelines(
            result_task_1_2(*solve_task_1_2())
        )
        print('tasks 1 and 2 solved')

        file.writelines(
            task3_result(solve_task3_4(3))
        )
        print('task 3 solved')
        file.writelines(
            task4_result(solve_task3_4(4))
        )
        print('task 4 solved')

        file.writelines(
            result_task5(*solve_task5())
        )
        print('task 5 solved')
