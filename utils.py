import datetime


def from_iso8601(time, remove_zone=False):
    if remove_zone:
        return datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%f%z").replace(tzinfo=None)
    else:
        return datetime.datetime.strptime(time, "%Y-%m-%dT%H:%M:%S.%f%z")


def iso_8601(time):
    return time.strftime("%Y-%m-%dT%H:%M:%S.%f%z")


def normal_time(time):
    return time.strftime('%d-%m-%y  %H:%M:%S')


def minimal(time):
    return time.strftime('%M:%S')


def origin(time):
    return time


def result_task_1_2(users_counter, output_byte_counter):
    user_result = []
    for user, count in users_counter:
        if user == '':
            user = 'Пользователь не указан'
        user_result.append(' {}: {} запросов'.format(user, count))
    user_result = '\n'.join(user_result)

    output_byte_result = []
    for output_byte, count in output_byte_counter:
        output_byte_result.append(' {}: {} едениц данных'.format(output_byte, count))
    output_byte_result = '\n'.join(output_byte_result)
    task1 = '''# Поиск 5ти пользователей, сгенерировавших наибольшее количество запросов
Решение1
{}
#Поиск 5ти пользователей, отправивших наибольшее количество данных
Решение2
{}'''.format(user_result, output_byte_result)
    return task1


def result_task5(ngrams_result, log_count):
    task5 = '''
#Рассматривая события сетевого трафика как символы неизвестного языка, найти 5 наиболее устойчивых N-грамм журнала событий (текста на неизвестном языке), (https://ru.wikipedia.org/wiki/N-грамм), где N=3-5. Тип символа задается квартетом user+src_port+dest_ip+dest_port.
Решение5\n'''

    result = ' {}. N={}, вероятность={}'
    for i, n_pair in enumerate(ngrams_result):
        gram, count = n_pair
        N = gram.count(';') + 1
        task5 += ' N-грамма {}. N={}, вероятность={}%. Упоминаний в тексте: {}\n {}\n'.format(i, N,
                                                                                              round(count / log_count,
                                                                                                    5) * 100, count,
                                                                                              gram.replace(';', '\n '))
        task5 += '\n'
    return task5


def task3_result(task_result):
    result = '''\n#Поиск регулярных запросов (запросов выполняющихся периодически) по полю src_user
Решение3
{}'''.format(task_result)
    return result


def task4_result(task_result):
    result = '''\n#Поиск регулярных запросов (запросов выполняющихся периодически) по полю src_ip
Решение4
{}'''.format(task_result)
    return result
